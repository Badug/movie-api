// Model for the Movie 
module.exports = (function moviesSchema () {
 
	var mongoose = require('../db').mongoose;
 
	var schema = {
		title: {type: String, required: true},
		genre: {type: String, required: true}
	};
	var collectionName = 'movie';
	var moviesSchema = mongoose.Schema(schema);
	var Movie = mongoose.model(collectionName, moviesSchema);
	
	return Movie;
})();