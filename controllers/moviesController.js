//This Controller deals with all the business Logic for the Movies API

function moviesController () {
	var Movie = require('../models/moviesSchema');
	var mongoose = require('mongoose'),
    ObjectId = mongoose.Types.ObjectId
    
	// Creating New Movie using POST
	this.addMovie = function (req, res, next) {
		var title = req.params.title;
		var genre = req.params.genre;
		
		Movie.create({title:title,genre:genre}, function(err, result) {
			if (err) {
				console.log(err);
				return res.send({'error':err});	
			}
			else {
        return res.send({'result':result,'status':'Movie successfully saved'});
      }
		});
	};

  // This function fetches all the movies as a single list by calling /movie
  this.getMovie = function (req, res, next) {

    Movie.find({}, function(err, result) {
      if (err) {
        console.log(err);
        return res.send({'error':err}); 
      }
      else {
        return res.send(result);
      }
    });
  };
  
  //view specific movie details
  this.viewMovie = function(req, res, next) {
    Movie.findById(new ObjectId(req.params.id), function(err, article) {
        if (err) {
            res.status(500);
            res.json({
                type: false,
                data: "Error occured: " + err
            })
        } else {
            if (article) {
                res.json({
                    type: true,
                    data: article
                })
            } else {
                res.json({
                    type: false,
                    data: "Movie: " + req.params.id + " not found"
                })
            }
        }
    });
};
  
  //update Movie Details
  this.updateMovie = function(req, res, next) {
    var updatedMovieModel = new Movie(req.body);
    Movie.findByIdAndUpdate(req.params.id, updatedMovieModel,{upsert:true}, function(err, article) {
        if (err) {
            res.status(500);
            res.json({
                type: false,
                data: "Error occured: " + err
            })
        } else {
            if (article) {
                res.json({
                    type: true,
                    data: article
                })
            } else {
                res.json({
                    type: false,
                    data: "Movie ID: " + req.params.id + " not found"
                })
            }
        }
    });
};

//delete Movie by ID
this.deleteMovie = function(req, res, next) {
    Movie.findByIdAndRemove(new Object(req.params.id), function(err, article) {
        if (err) {
            res.status(500);
            res.json({
                type: false,
                data: "Error occured: " + err
            })
        } else {
            res.json({
                type: true,
                data: "Movie: " + req.params.id + " deleted successfully"
            })
        }
    });
}; 

return this;

};

module.exports = new moviesController();