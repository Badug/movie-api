var restify = require('restify');
var config = require('./config');
var movie = require('./controllers/moviesController');
var server = restify.createServer({name:'MOVIES-api'});
 
server.use(restify.fullResponse());
server.use(restify.bodyParser());
server.use(restify.queryParser());


//define all routes 

//this returns a welcome message if no parameters are defined on the url
server.get('/', function(request, response, next) {
		return response.send("WELCOME TO OUR MOVIES API");
	});
 
	server.post("/movie", movie.addMovie);// this creates a new movie
    server.get("/movie", movie.getMovie); // this gets all the movies list
    server.get({path: "/movie/:id", version: "1.0.0"},movie.viewMovie);// this gets a specific movie details given the movie id
    server.put("/movie/:id", movie.updateMovie);// this updates a specific movie given the movie id
    server.del("/movie/:id", movie.deleteMovie);// this deletes a specific movie given the movie id
 
 
 //start server and output the port at which the server is running on
server.listen(config.port, function() {
	console.log('server listening on port number', config.port);
	
});


